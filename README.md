Cucumber + Webdriver.js Demo
===================================

This is an example Cucumber.js + Webdriver.js setup. It uses the [Polymer demo store](https://shop.polymer-project.org/) 
as a test target and includes support for working with shadow roots.

## Useful links

* [Cucumber.js](https://github.com/cucumber/cucumber-js)
* [Gherkin](https://github.com/cucumber/cucumber/wiki/Gherkin)
* [Cucumber expressions](https://github.com/cucumber/cucumber/tree/master/cucumber-expressions)
* [Webdriver.js API](https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_WebDriver.html)

## Setup

```bash
npm install
```

## Run

```bash
npm test
```

**Profiles**

By default this setup is configured to run the specs using Chrome in headless mode.

The file `./cucumber.js` is where additional profiles can be configured. It contains some custom, preconfigured profiles
to make it easy to run the specs in other browsers. You can launch with a given profile using
the `-p` argument:

```bash
# Chrome (not headless)
npm test -- -p chome

# Firefox (geckodriver)
npm test -- -p firefox
```

See [these docs](https://github.com/cucumber/cucumber-js/blob/master/docs/cli.md#profiles) for more about profiles.

## Files and directories

Most of this repo is structured in an idomatic Cucmber.js way:

| File/Directory | Description |
| --------------------------- | --------------------------------------------------- |
| `features`                  | `*.feature`  files live here.                       |
| `features/support`          | Extra support files expected by Cucumber (`world.js` and `hooks.js`)            |
| `features/step_definitions` | JS files that contain the Cucumber step definitions |
| `cucumber.js`               | Cucumber's profile configuration file           |
 
## Features

Feature files contain specs written in Cucumber's Gherkin syntax:

```gherkin
Background:
  Given I am on the Polymer shop homepage
  When I click the button with label "Ladies Outerwear Shop Now"
  And I select the product "Ladies Colorblock Wind Jacket"

Scenario: Check the price
  Then the price should be $45.90

Scenario: Add the anorak to my cart
  When I click the button with label "Add this item to cart"
  Then the cart icon should show 1 item
```

Gherkin provides a nice structure to organize test code:

**Given** steps set up the environment for the test,
**When** steps define user actions,
**Then** steps are the assertions.

## Step definitions

Step definitions (`Given`, `When` and `Then`) contain the associated Webdriver.js test code driven by 
Gherkin specs. Cucumber will automatically look for step definitions in the files located in the 
`features/step_definitions` directory. The `given.js`, `when.js` and `then.js` organization of 
step definitions here, is just my preference. 

Almost every action you perform with Webdriver.js is async. Until recently, this made it's API
very cumbersome to work with, but *async await* syntax has improved the situation significantly.

```javascript
When('I click the button with label {string}', async function(ariaLabel) {
  const selector = `shop-button > [aria-label="${ariaLabel}"]`;
  await this.waitForThroughShadowRoots('css', selector);
  const button = await this.findOneThroughShadowRoots('css', selector, { closest: 'shop-button' });
  await button.click();
});

Then(/^the cart icon should show (\d+) items?$/, async function(expectedNumItems) {
  // let the UI update
  this.sleep(100);
  const selector = '.cart-badge';
  await this.waitForThroughShadowRoots('css', selector);
  const el = await this.findOneThroughShadowRoots('css', selector);
  const actualNumItems = await el.getText();
  expect(actualNumItems).toEqual(expectedNumItems + '');
});
```

### Some notes on step definitions

* The first argument of a step definition is the **text pattern** that should match one or more Gherkin steps. This can be 
written using a [Cucumber expression](https://github.com/cucumber/cucumber/tree/master/cucumber-expressions)
or a regular expression. I usually prefer the simplicity of a cucmber expression, but sometimes one needs a
regex for more advanced pattern matching.

* The `When` step above is a good example of getting **reuse out of step definitions**. When you first start writing 
Cucumber specs, for each Gherkin step, you will write an assocaited JS step definition. But as the step definition 
library matures you will find you only need to write the Gherkin because the JS is already there. This 
example shows how any button in the Polymer store can be clicked with one reusable step definition.

* These examples **wait for** an element to be present on a page before getting it via the async calls `this.waitFor...`
and `this.findOne...`. I can't decide whether it's better to explictly wait or not... but these functions could be consolidated to automatically always wait before finding. See the [world](#world) stuff below.

* You can use any assertion API. Here I have chosen [Jest's `expect` API](https://facebook.github.io/jest/docs/en/expect.html), but Chai or whatever would also work.

<a id=world></a>

## Hooks

You can hook into the Cucumber lifecycle via `features/support/hooks.js`. 

In this setup I've configured Cucumber to save a screenshot whenever a scenario fails.

## World

Cucumber.js has the concept of a *world* class, defined in `features/support/world.js` who's 
single instance is used as the bound context (`this`) when running step definitions functions:

```javascript
// world.js 
const { setWorldConstructor } = require('cucumber')

class World {
  hello() {
    console.log('hello');
  }
}

setWorldConstructor(World);

// a step definition
Given('The sky is blue', function() {
  this.hello();
})
```

Incidentally, step definitions cannot be written as fat arrows, because they must be bound to the world context.

### Webdriver Facade API

In this example setup, I use the *world* class as a convenient place to define a custom facade API over the Webdriver.js API, which
does a lot, but can be quite verbose to use directly all the time. For example:

```javascript
const el = await this.waitForCss(selector);

// vs

const el = await this.driver.wait(webdriver.until.elementLocated(webdriver.By.css(css)));
```

This API is just an example to get started. It can change to suit developer preferences or include more domain
specific abstractions. For example, a set of Webdriver commands to login to an application. Also, it doesn't stop you from using the Webdriver.js API directly.

### Working with Shadow DOM

The API defined in `world.js` also contains some custom helper functions for finding elements 
nested within shadow roots:

```javascript
const buttons = await this.findThroughShadowRoots('css', '[aria-label="Menswear"]');
const button = await this.findOneThroughShadowRoots('css', '[aria-label="Menswear"]');
```

These functions use `this.driver.executeScript()` to execute Javascript in the browser that traverses
through shadow roots and returns Selenium `WebElemnent`(s).

The implementation here isn't the most efficient code in the world, but in the context of functional testing its unlikely to be a problem. If it is, less generic code could be written to target nested elements more directly.

### Selecting by text content

It might be convenient to find an elemnent, via its text content. Typically, you would do 
this via Selenium's XPath locators (you can't select by text 
content with CSS selectors unless it's `:empty`). But given we use `executeScript`
to find elements by traversing shadow roots, it is not possible to locate elements 
by XPath in IE11. 

However, to compensate for this, I have added special support for matching by text content in the 
`findThroughShadowRoots` functions:

```javascript
findOneThroughShadowRoots('text', 'I am the text content');
```

### API

I've just documented here the shadow root functions, as they are a bit more tricky. There are more 
functions defined in `world.js`.

#### findThroughShadowRoots(findStrategy, findValue, options): WebElement[]

##### findStrategy: string

One of:

* **"css"** find by css selector 
* **"name"** find by name
* **"id"** find by id
* **"text"** find by the text content of a node

##### findValue: string

A value associated with the desired *findStrategy* CSS selector, id, name or text.

##### options.closest: string (css selector)

Select the closet ancestor matching the found element, using a CSS selector. E.g.

```html
<shop-button>
  <span>
    <a aria-label="Click me">Click me</a>
  </span>
</shop-button>
```

```javascript
const button = await this.findThroughShadowRoots('css', '[aria-label="Click me"]', { closest: 'shop-button'});
await button.click();
// button === <shop-button>
```

This is useful when a child of an element you want to find only contains the information you can select by.

**Also see:**

`findOneThroughShadowRoots(findStrategy, findValue, options)`
`waitForThroughShadowRoots(findStrategy, findValue)`
