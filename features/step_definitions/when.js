const { When } = require('cucumber');

When('I click the button with label {string}', async function(ariaLabel) {
  const selector = `shop-button > [aria-label="${ariaLabel}"]`;
  await this.waitForThroughShadowRoots('css', selector);
  const button = await this.findOneThroughShadowRoots('css', selector, { closest: 'shop-button' });
  await button.click();
});

When('I select the product {string}', async function(productTitle) {  
  await this.waitForThroughShadowRoots('text', productTitle);
  const button = await this.findOneThroughShadowRoots('text', productTitle, { closest: 'a' });
  await button.click();
});
