const { Then } = require('cucumber');
const expect = require('jest-matchers');

Then(/^the price should be (.+)$/, async function(expectedPrice) {
  const selector = '.detail .price';
  await this.waitForThroughShadowRoots('css', selector);
  const el = await this.findOneThroughShadowRoots('css', selector);
  const actualPrice = await el.getText();
  expect(actualPrice).toEqual(expectedPrice);
});

Then(/^the cart icon should show (\d+) items?$/, async function(expectedNumItems) {
  // let the UI update
  this.sleep(100);
  const selector = '.cart-badge';
  await this.waitForThroughShadowRoots('css', selector);
  const el = await this.findOneThroughShadowRoots('css', selector);
  const actualNumItems = await el.getText();
  expect(actualNumItems).toEqual(expectedNumItems + '');
});
