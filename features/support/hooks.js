const { writeFile, mkdirSync, existsSync } = require('fs');
const { join } = require('path');
const { setDefaultTimeout, After, BeforeAll } = require('cucumber')

// timeout
setDefaultTimeout(1000 * 60);

// quit after every scenario
// might be better to quit in AfterAll instead, but this will require more
// work to expose the driver instance nicely
After(async function(scenario) {
  await this.driver.quit();
});

// screenshot stuff
const screenshotsDir = join(process.cwd(), 'screenshots');
if(!existsSync(screenshotsDir)) {
  mkdirSync(screenshotsDir);
}

let currentScreenshotDir;
BeforeAll(function() {
  const date = new Date();
  currentScreenshotDir = join(screenshotsDir, `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`);
})

After(async function(info) {
  // take screenshots of failed scenarios
  if(info.result.status === 'failed') {
    if(!existsSync(currentScreenshotDir)) {
      mkdirSync(currentScreenshotDir);
    }

    const imageData = await this.driver.takeScreenshot();
    const base64Data = imageData.replace(/^data:image\/png;base64,/,"")
    const { name } = info.pickle;
    const out = join(currentScreenshotDir, `${name.toLowerCase()}.png`.replace(/ /g, '-'))
    writeFile(out, base64Data, 'base64', err => {
        if(err) { 
          console.error(err); 
        }
    });
  }
});