const chrome = require('selenium-webdriver/chrome');
const webdriver = require('selenium-webdriver');
const { setWorldConstructor } = require('cucumber')

class World {
  constructor({ parameters }) {
    const {
      driver = 'chrome',
      userPreferences = {},
      args = ['--headless', '--disable-gpu'],
    } = parameters;

    // chrome options: only applicable if the driver is chrome, otherwise redundant
    let chromeOpts = new chrome.Options();
    if (userPreferences) {
      chromeOpts.setUserPreferences(userPreferences);
    }
    chromeOpts.addArguments(...args);

    this.driver = new webdriver.Builder()
      .forBrowser(driver)
      .setChromeOptions(chromeOpts)
      .build();

    // shortcut functions
    this.refresh = () => this.driver.navigate().refresh();
    this.get = url => this.driver.get(url);
    this.sleep = timeout => this.driver.sleep(timeout);
    this.wait = condition => this.driver.wait(condition);
    this.waitForName = name => this.driver.wait(webdriver.until.elementLocated(webdriver.By.name(name)));
    this.waitForCss = css => this.driver.wait(webdriver.until.elementLocated(webdriver.By.css(css)));
    this.waitForXpath = xpath => this.driver.wait(webdriver.until.elementLocated(webdriver.By.xpath(xpath)));
    this.findOneByCss = css => this.waitForCss(css).then(() => this.driver.findElement(webdriver.By.css(css)));
    this.findOneByName = name => this.driver.findElement(webdriver.By.name(name));
    this.findOneBymatchFn = matchFn => this.driver.findElement(matchFn);
    this.findBymatchFn = matchFn => this.driver.findElements(matchFn);
    this.findByCss = css => this.driver.findElements(webdriver.By.css(css));
    this.findByXpath = xpath => this.driver.findElements(webdriver.By.xpath(xpath));
    this.executeScript = (...args) => this.driver.executeScript(...args);

    this.findThroughShadowRoots = (matchingStrategy, matchingValue, opts = {}) => 
      this.driver.executeScript(findThroughShadowRoots, matchingStrategy, matchingValue, opts);
    this.findOneThroughShadowRoots = (matchingStrategy, matchingValue, opts = {}) => 
      this.driver.executeScript(findThroughShadowRoots, matchingStrategy, matchingValue, Object.assign({}, opts, { limit: 1}));
      
    this.waitForThroughShadowRoots = (matchingStrategy, matchingValue) => {
      return this.driver.wait(async () => {
        return await this.findOneThroughShadowRoots(matchingStrategy, matchingValue);
      });
    }
  }

  info(msg) {
    console.log(`\ninfo: ${msg}\n`);
  }
}
setWorldConstructor(World);

// deliberately ES5 (IE11 compatible), this will run in the browser
function findThroughShadowRoots() {
  var matchingStrategy = arguments[0];
  var matchingValue = arguments[1];
  var opts = arguments[2];

  // recursively look for an element that is matched by the given function (matchFn)
  function findMatchingElements(parent, matchFn) {
    var nodes = Array.prototype.slice.call(parent.querySelectorAll('*'));
  
    return nodes.reduce(function(inputs, el) {
      if (matchFn(el)) {
        inputs.push(el);
      } else if (el.shadowRoot) {
        inputs = inputs.concat(findMatchingElements(el.shadowRoot, matchFn));
      }
      return inputs;
    }, []);
  };

  function findClosest(el, selector) {  
    while (el = el.parentElement || (el.parentNode && el.parentNode.host) || el.parentNode) {
      var matchesSelector = el.matches || el.matchesSelector;  
      if(!el || el === document) {
        return null;
      } else if(matchesSelector && matchesSelector.call(el, selector)) {
        return el;
      }
    }
  }

  function trim(str) {
    return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  }

  function matchFn(el) {
    if(matchingStrategy === 'css') {
      return (el.matches || el.matchesSelector).call(el, matchingValue)
    } else if(matchingStrategy === 'id') {
      return el.id && el.id === matchingValue;
    } else if(matchingStrategy === 'name') {
      return el.name && el.name === matchingValue;
    } else if(matchingStrategy === 'text') {
      return trim(el.textContent) === trim(matchingValue);
    }
  }

  var elements = findMatchingElements(document.body, matchFn);

  // return matched elements, applying various options
  var limit = opts.limit;
  var closest = opts.closest;
  if(typeof limit === 'number' && limit > 0) {    
    var el = elements[limit - 1];
    if(el) {
      return closest ? findClosest(el, closest) : el;
    }
  } else {
    return closest ? elements.map(function(el) {
      return findClosest(el, closest);
    }) : elements;
  }
}
