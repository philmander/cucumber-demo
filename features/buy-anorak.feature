Feature: Buy an anorak
  As shopper
  I want to buy a Google anorak
  So I can brave the Dutch weather and look like a nerd

Background:
  Given I am on the Polymer shop homepage
  When I click the button with label "Ladies Outerwear Shop Now"
  And I select the product "Ladies Colorblock Wind Jacket"

Scenario: Check the price
  Then the price should be $45.90

Scenario: Add the anorak to my cart
  When I click the button with label "Add this item to cart"
  Then the cart icon should show 1 item