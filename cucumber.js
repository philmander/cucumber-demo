const profiles = {
  chrome: {
      driver: 'chrome',
      args: [],
      userPreferences: {
          'credentials_enable_service': false,
          'profile.password_manager_enabled': false,
      }
  },
  headless: {
      driver: 'chrome',
      args: ['--headless', '--disable-gpu'],
      userPreferences: {
          'credentials_enable_service': false,
          'profile.password_manager_enabled': false,
      }
  },
  firefox: {
      driver: 'firefox'
  },
};

module.exports = {
  chrome: `--world-parameters '${JSON.stringify(profiles.chrome)}'`,
  headless: `--world-parameters '${JSON.stringify(profiles.headless)}'`,
  firefox: `--world-parameters '${JSON.stringify(profiles.firefox)}'`,
};